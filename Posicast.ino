#include <Servo.h>

#define LED_POSI_R  13
#define LED_POSI_G  47
#define LED_UP      43
#define LED_DN      45

#define BTN_POSI    49
#define BTN_UP      53
#define BTN_DN      51

#define SERVO_PIN   9

#define DEBOUNCE    500

#define REF_UP      0
#define REF_DN      180
//#define TRANS_DEL   1327/2
#define TRANS_DEL   (1436/2)

bool posi_enabled = false;

Servo servo;

int angle_ref = (REF_UP+REF_DN)/2;

unsigned long last_btn_time = 0;

void setup() {
  Serial.begin(115200);
  
  pinMode(LED_POSI_R, OUTPUT);
  pinMode(LED_POSI_G, OUTPUT);
  pinMode(LED_UP,     OUTPUT);
  pinMode(LED_DN,     OUTPUT);
  
  pinMode(BTN_POSI,   INPUT_PULLUP);
  pinMode(BTN_UP,     INPUT_PULLUP);
  pinMode(BTN_DN,     INPUT_PULLUP);

  servo.attach(SERVO_PIN);
  servo.write(angle_ref);
  switchPosicast(true);
}

void loop() {
  int last_ref = angle_ref;
  buttons();

  if(Serial.available()){
    switch(Serial.read()){
      case 'E':
        switchPosicast(true);
        break;
      case 'D':
        switchPosicast(false);
        break;
      case 'A':
        angle_ref = Serial.parseInt();
        Serial.print("Set by PC to ");
        Serial.println(angle_ref); 
        break;   
    }
  }

  int diff = angle_ref-last_ref;
  if(diff!=0){
    Serial.print("REF cahnged to ");
    Serial.println(angle_ref);
    digitalWrite(LED_UP, false);
    digitalWrite(LED_DN, false);
    if(posi_enabled){
      servo.write(last_ref+(diff/2));
      Serial.print("First part of move ");
      Serial.println(last_ref+diff);
      delay(TRANS_DEL);
    }
    Serial.print("Second (or the only one) part of move ");
    Serial.println(angle_ref);
    servo.write(angle_ref);
  }
  digitalWrite(LED_UP, angle_ref == REF_UP);
  digitalWrite(LED_DN, angle_ref == REF_DN);   
}

void buttons(){
  if(!digitalRead(BTN_UP)) angle_ref = REF_UP;
  if(!digitalRead(BTN_DN)) angle_ref = REF_DN;

  //Obsluha prepnuti rezimu
  static bool last_btn = false;
  bool btn = !digitalRead(BTN_POSI);
  if((btn!=last_btn) && btn && ((last_btn_time + DEBOUNCE) < millis())){
    switchPosicast(!posi_enabled);
    last_btn_time = millis();
  }
}

void switchPosicast(bool state){
  posi_enabled = state;
  digitalWrite(LED_POSI_R, !posi_enabled);
  digitalWrite(LED_POSI_G,  posi_enabled);
  Serial.println(posi_enabled?"Posicast enabled":"Posicast disabled");
}
